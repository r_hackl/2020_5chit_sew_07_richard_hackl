﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooBar
{
    class Program
    {
        static void Init()
        {
            MyDBContext c = new MyDBContext();
            c.Foos.Add(new Foo() { license = "mogalicense" });
            c.Foos.Add(new Foo() { license = "mogalicense" });
            c.Bars.Add(new Bar() { hwid = "mogahwid" });
            c.Bars.Add(new Bar() { hwid = "mogahwid" });
            c.SaveChanges();

        }
        static void Main(string[] args)
        {
            Init();
        }
    }
}
