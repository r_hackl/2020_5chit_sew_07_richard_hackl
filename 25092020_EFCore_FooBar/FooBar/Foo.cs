﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooBar
{
    public class Foo
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string license { get; set; }
    }
}
