﻿using _13122020_ASP_XXXLutz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _13122020_ASP_XXXLutz.Domain
{
    public class PersonRepo
    {
        public static List<Employee> WhereEmployeeIsWarehouseWorker(MyDbContext ctx)
        {
            return ctx.Employees.Where(a => a.Job == EJob.WarehouseWorker).ToList();
        }

        public static List<Customer> WhereCustomerHasMultipleOrders(MyDbContext ctx)
        {
            return ctx.Customers.Where(a => a.Orders.Count() > 1).ToList();
        }

        public static List<Employee> WhereEmployeeWorksInFurnitureShop(MyDbContext ctx)
        {
            List<Employee> sellers = ctx.Employees.Where(a => a.Job == EJob.Seller).ToList();
            List<Employee> hod = ctx.Employees.Where(a => a.Job == EJob.HeadOfDepartment).ToList();
            List<Employee> cashiers = ctx.Employees.Where(a => a.Job == EJob.Cashier).ToList();

            List<Employee> FurnitureShopWorkers = sellers.AsEnumerable().Concat(hod).ToList();
            FurnitureShopWorkers = sellers.AsEnumerable().Concat(cashiers).ToList();

            return FurnitureShopWorkers;
        }

        //public static FurnitureShop WhereFurnitureShopHasFurnitureShopWorker(MyDbContext ctx, FurnitureShopWorker furnitureShopWorker)
        //{
        //    return ctx.FurnitureShops.Select(s => s.FurnitureShopWorkers).Contains(furnitureShopWorker);
        //}
    }
}
