﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

using _13122020_ASP_XXXLutz.Model;

namespace _13122020_ASP_XXXLutz
{
    public class MyDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Furniture> Furnitures { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<WarehouseWarehouseWorker> WarehouseWarehouseWorkers { get; set; }
        public DbSet<FurnitureShop> FurnitureShops { get; set; }
        public DbSet<FurnitureShopWorker> FurnitureShopWorkers { get; set; }

        public MyDbContext()
        {

        }
        public MyDbContext(DbContextOptions<MyDbContext> options)
            : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\r1k\Documents\xxxlutzmvc.mdf;Integrated Security=True;Connect Timeout=30");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WarehouseWarehouseWorker>().HasKey(www => new { www.WarehouseWorkerId, www.WarehouseId });

            //m - n

            modelBuilder.Entity<WarehouseWarehouseWorker>()
                .HasOne<Warehouse>(wh => wh.Warehouse)
                .WithMany(w => w.WarehouseWarehouseWorkers)
                .HasForeignKey(wh => wh.WarehouseId);

            modelBuilder.Entity<WarehouseWarehouseWorker>()
                .HasOne<WarehouseWorker>(whw => whw.WarehouseWorker)
                .WithMany(w => w.WarehouseWarehouseWorkers)
                .HasForeignKey(whw => whw.WarehouseWorkerId);

            // shadow property
            modelBuilder.Entity<Employee>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Employee>().Property<DateTime>("UpdatedDate");
        }

        public static void Seed()
        {
            MyDbContext mdbc = new MyDbContext();

            mdbc.Customers.Add(new Customer() { HasCustomerCard = false, Name = "Seep" });
            mdbc.Customers.Add(new Customer() { HasCustomerCard = true, Name = "Korl" });
            mdbc.Customers.Add(new Customer() { HasCustomerCard = false, Name = "Bauii" });
            mdbc.Customers.Add(new Customer() { HasCustomerCard = true, Name = "Andi" });
            mdbc.Customers.Add(new Customer() { HasCustomerCard = false, Name = "Bernhard" });
            mdbc.Customers.Add(new Customer() { HasCustomerCard = true, Name = "Georg" });

            mdbc.Furnitures.Add(new Furniture() { Collection = "new Fall Collection", Price = 2399.99, Type = EFurnitureType.Cupboard });
            mdbc.Furnitures.Add(new Furniture() { Collection = "new Spring Collection", Price = 1499.99, Type = EFurnitureType.Chair });
            mdbc.Furnitures.Add(new Furniture() { Collection = "new Winter Collection", Price = 1599.99, Type = EFurnitureType.Table });
            mdbc.Furnitures.Add(new Furniture() { Collection = "last Fall Collection", Price = 2399.99, Type = EFurnitureType.Cupboard });
            mdbc.Furnitures.Add(new Furniture() { Collection = "last Winter Cllection", Price = 1299.99, Type = EFurnitureType.Chair });

            mdbc.Employees.Add(new Employee() { EmploymentDate = DateTime.Now, Job = EJob.Seller, Name = "Hauns" });
            mdbc.Employees.Add(new Employee() { EmploymentDate = DateTime.Now, Job = EJob.WarehouseWorker, Name = "Hofi" });
            mdbc.Employees.Add(new Employee() { EmploymentDate = DateTime.Now, Job = EJob.Seller, Name = "David" });
            mdbc.Employees.Add(new Employee() { EmploymentDate = DateTime.Now, Job = EJob.WarehouseWorker, Name = "Huawa" });
            mdbc.Employees.Add(new Employee() { EmploymentDate = DateTime.Now, Job = EJob.Seller, Name = "Clemi" });

            List<Furniture> frntrs = new List<Furniture> { };
            frntrs.Add(new Furniture() { Collection = "new Fall Collection", Price = 2399.99, Type = EFurnitureType.Cupboard });
            frntrs.Add(new Furniture() { Collection = "last Fall Collection", Price = 2399.99, Type = EFurnitureType.Cupboard });
            frntrs.Add(new Furniture() { Collection = "last Winter Cllection", Price = 1299.99, Type = EFurnitureType.Chair });

            mdbc.Warehouses.Add(new Warehouse() { Address = "3799  Willow Greene Drive", JoinDate = DateTime.Now, Furnitures = frntrs });
            mdbc.Warehouses.Add(new Warehouse() { Address = "1207  Crowfield Roade", JoinDate = DateTime.Now, Furnitures = frntrs });
            mdbc.Warehouses.Add(new Warehouse() { Address = "210  Henery Street", JoinDate = DateTime.Now, Furnitures = frntrs });
            mdbc.Warehouses.Add(new Warehouse() { Address = "218  Henery Street", JoinDate = DateTime.Now, Furnitures = frntrs });
            mdbc.Warehouses.Add(new Warehouse() { Address = "4635  Neville Street", JoinDate = DateTime.Now, Furnitures = frntrs });

            List<FurnitureShopWorker> shopWorkers = new List<FurnitureShopWorker> { };
            shopWorkers.Add(new FurnitureShopWorker() { AufgabenBereich = "Textil", Name = "Honz", OvertimeHours = 34.5f, Salary = 1840 });

            mdbc.FurnitureShops.Add(new FurnitureShop() { Address = "1207  Crowfield Roade", FurnitureShopWorkers = shopWorkers });
            mdbc.FurnitureShops.Add(new FurnitureShop() { Address = "210  Henery Street", FurnitureShopWorkers = shopWorkers });
            mdbc.FurnitureShops.Add(new FurnitureShop() { Address = "3799  Willow Greene Drive", FurnitureShopWorkers = shopWorkers });
            mdbc.FurnitureShops.Add(new FurnitureShop() { Address = "218  Henery Street", FurnitureShopWorkers = shopWorkers });
            mdbc.FurnitureShops.Add(new FurnitureShop() { Address = "1207  Wallstreet Roade", FurnitureShopWorkers = shopWorkers });

            mdbc.SaveChanges();
        }
    }
}
