﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace _13122020_ASP_XXXLutz.Model
{
    public class WarehouseWarehouseWorker { 
        public int WarehouseWorkerId { get; set; }
        public WarehouseWorker WarehouseWorker { get; set; }

        public int WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }
    }
}
