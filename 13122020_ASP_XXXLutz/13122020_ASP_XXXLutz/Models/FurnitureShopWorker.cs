﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace _13122020_ASP_XXXLutz.Model
{
    [Table("FurnitureShopWorker")]
    public class FurnitureShopWorker : Employee
    {
        //n - m relation
        public string AufgabenBereich { get; set; }
        public float Salary { get; set; }

        [Column("OVERTIME")]
        public float OvertimeHours { get; set; }
    }
}
