﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace _13122020_ASP_XXXLutz.Model
{
    public class Warehouse : AEntity
    {
        [Required]
        public string Address { get; set; }
        //n-m relation
        public IList<WarehouseWarehouseWorker> WarehouseWarehouseWorkers { get; set; }
        public ICollection<Furniture> Furnitures { get; set; }
        public DateTime JoinDate { get; set; }
    }
}
