﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _210208_EFCZoo.Migrations
{
    public partial class Zoo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ACage",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SqMeters = table.Column<double>(type: "float", nullable: false),
                    AnimalEscaped = table.Column<int>(type: "int", nullable: false),
                    Location = table.Column<int>(type: "int", nullable: false),
                    Discriminator = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ACage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AAnimal",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Age = table.Column<int>(type: "int", nullable: false),
                    IsMale = table.Column<bool>(type: "bit", nullable: false),
                    CageId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AAnimal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AAnimal_ACage_CageId",
                        column: x => x.CageId,
                        principalTable: "ACage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Gazelle",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    JumpLenght = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gazelle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gazelle_AAnimal_Id",
                        column: x => x.Id,
                        principalTable: "AAnimal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Harpy",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    HowlLoudnes = table.Column<int>(type: "int", nullable: false),
                    InPack = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Harpy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Harpy_AAnimal_Id",
                        column: x => x.Id,
                        principalTable: "AAnimal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lion",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    GazellesEaten = table.Column<int>(type: "int", nullable: false),
                    ManeLenght = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lion_AAnimal_Id",
                        column: x => x.Id,
                        principalTable: "AAnimal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Shark",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    TouthCont = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shark", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Shark_AAnimal_Id",
                        column: x => x.Id,
                        principalTable: "AAnimal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tortoise",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    ShellDiameter = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tortoise", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tortoise_AAnimal_Id",
                        column: x => x.Id,
                        principalTable: "AAnimal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TortoiseWaterCage",
                columns: table => new
                {
                    cagesId = table.Column<long>(type: "bigint", nullable: false),
                    tortoisesId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TortoiseWaterCage", x => new { x.cagesId, x.tortoisesId });
                    table.ForeignKey(
                        name: "FK_TortoiseWaterCage_ACage_cagesId",
                        column: x => x.cagesId,
                        principalTable: "ACage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TortoiseWaterCage_Tortoise_tortoisesId",
                        column: x => x.tortoisesId,
                        principalTable: "Tortoise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AAnimal_CageId",
                table: "AAnimal",
                column: "CageId");

            migrationBuilder.CreateIndex(
                name: "IX_TortoiseWaterCage_tortoisesId",
                table: "TortoiseWaterCage",
                column: "tortoisesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Gazelle");

            migrationBuilder.DropTable(
                name: "Harpy");

            migrationBuilder.DropTable(
                name: "Lion");

            migrationBuilder.DropTable(
                name: "Shark");

            migrationBuilder.DropTable(
                name: "TortoiseWaterCage");

            migrationBuilder.DropTable(
                name: "Tortoise");

            migrationBuilder.DropTable(
                name: "AAnimal");

            migrationBuilder.DropTable(
                name: "ACage");
        }
    }
}
