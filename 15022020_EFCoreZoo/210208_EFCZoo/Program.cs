﻿using _210208_EFCZoo.Models;
using _210208_EFCZoo.Models.Animals;
using _210208_EFCZoo.Models.Cages;
using System;
using System.Linq;

namespace _210208_EFCZoo
{
    class Program
    {
        static void Main(string[] args)
        {
            ZooDBContext context = new ZooDBContext();
            FillDB(context);
        }
        static void FillDB(ZooDBContext context)
        {
            Random rng = new Random();

            for (int i = 0; i < 5; i++)
            {
                context.FreindlyCages.Add(new FriendlyCage() { AnimalEscaped = rng.Next(0, 25), Location = ELOCATION.SAVANNAH, SqMeters = rng.NextDouble() });
                context.preditorCages.Add(new PreditorCage() { AnimalEscaped = rng.Next(0, 25), Location = ELOCATION.SAVANNAH, SqMeters = rng.NextDouble() });
                context.WaterCages.Add(new WaterCage() { AnimalEscaped = rng.Next(0, 25), Location = ELOCATION.POOL, SqMeters = rng.NextDouble() });
                context.SaveChanges();
            }
            
            for (int i = 0; i < 10; i++)
            {
                context.Gazelles.Add(new Gazelle() { Name = "Gazelle "+i, Age = rng.Next(1,12), IsMale = rng.Next(0,2) == 0, JumpLenght = rng.NextDouble(), Cage = context.FreindlyCages.Where( cg => cg.Id == 1 ).First() });
                context.Harpies.Add(new Harpy() { Name = "Harpy " + i, Age = rng.Next(1, 8), InPack = rng.Next(0, 2) == 0, HowlLoudnes = rng.Next(1, 10), IsMale = rng.Next(0, 2) == 0, Cage = context.preditorCages.First(cg => cg.Id == 2) });
                context.Lions.Add(new Lion() { Name = "Lion " + i, Age = rng.Next(1, 25), GazellesEaten = rng.Next(0, 100), IsMale = rng.Next(0, 2) == 0, ManeLenght = rng.NextDouble(), Cage = context.preditorCages.First(cg => cg.Id == 2)  });
                context.Sharks.Add(new Shark() { Name = "Shark "+i, Age = rng.Next(1, 15), IsMale = rng.Next(0, 2) == 0, TouthCont = rng.Next(100, 150), Cage = context.WaterCages.First(cg => cg.Id == 4) });
                context.Tortoises.Add(new Tortoise() { Name = "Tortoise " + i, Age = rng.Next(1, 100), IsMale = rng.Next(0, 2) == 0, ShellDiameter = rng.NextDouble(), Cage = context.WaterCages.First(cg => cg.Id == 4), cages = (System.Collections.Generic.ICollection<WaterCage>)context.WaterCages.Where(cg => cg.Id < 4).ToList() });
            }
            context.SaveChanges();
        }
    }
}
