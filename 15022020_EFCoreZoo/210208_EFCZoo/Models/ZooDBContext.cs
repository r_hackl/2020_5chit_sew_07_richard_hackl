﻿using _210208_EFCZoo.Models.Animals;
using _210208_EFCZoo.Models.Cages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace _210208_EFCZoo.Models
{
    public class ZooDBContext : DbContext 
    {
        public DbSet<Gazelle> Gazelles { get; set; }
        public DbSet<Harpy> Harpies { get; set; }
        public DbSet<Lion> Lions { get; set; }
        public DbSet<Shark> Sharks { get; set; }
        public DbSet<Tortoise> Tortoises { get; set; }

        public DbSet<FriendlyCage> FreindlyCages { get; set; }
        public DbSet<PreditorCage> preditorCages { get; set; }
        public DbSet<WaterCage> WaterCages { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;
                                Database=Zoo;Trusted_Connection=True;");
        }
    }
}
