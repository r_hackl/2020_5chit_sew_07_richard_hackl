﻿using _210208_EFCZoo.Models.Animals;
using System;
using System.Collections.Generic;
using System.Text;

namespace _210208_EFCZoo.Models.Cages
{
    public enum ELOCATION { POOL, SAVANNAH}
    public abstract class ACage: AEntity
    {
        public double SqMeters { get; set; }

        public int AnimalEscaped { get; set; }

        public ELOCATION Location { get; set; }

        public ICollection<AAnimal> Animals { get; set; }
    }
}
