﻿using _210208_EFCZoo.Models.Animals;
using System;
using System.Collections.Generic;
using System.Text;

namespace _210208_EFCZoo.Models.Cages
{
    public class WaterCage : ACage
    {
        public ICollection<Tortoise> tortoises { get; set; }
    }
}
