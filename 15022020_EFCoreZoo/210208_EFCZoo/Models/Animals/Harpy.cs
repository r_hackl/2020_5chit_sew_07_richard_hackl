﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _210208_EFCZoo.Models.Animals
{

    [Table("Harpy")]
    public class Harpy : AAnimal
    {
        public int HowlLoudnes { get; set; }
        public bool InPack { get; set; }
    }
}
