﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _210208_EFCZoo.Models.Animals
{
    [Table("Shark")]
    public class Shark : AAnimal
    {
        public int TouthCont { get; set; }
    }
}
