﻿using _210208_EFCZoo.Models.Cages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _210208_EFCZoo.Models.Animals
{
    [Table("Tortoise")]
    public class Tortoise : AAnimal
    {
        public double ShellDiameter { get; set; }
        public ICollection<WaterCage> cages { get; set; }

    }
}
