﻿using _210208_EFCZoo.Models.Cages;
using System;
using System.Collections.Generic;
using System.Text;

namespace _210208_EFCZoo.Models.Animals
{
    public abstract class AAnimal: AEntity
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool IsMale { get; set; }
        public ACage Cage { get; set; }
    }
}
