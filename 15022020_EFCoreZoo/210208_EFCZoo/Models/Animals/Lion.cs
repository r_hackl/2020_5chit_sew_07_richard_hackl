﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _210208_EFCZoo.Models.Animals
{
    [Table("Lion")]
    public class Lion : AAnimal
    {
        public int GazellesEaten { get; set; }
        public double ManeLenght { get; set; }
    }
}
