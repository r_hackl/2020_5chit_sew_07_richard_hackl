﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _210208_EFCZoo.Models
{
    public abstract class AEntity
    {
        [Key]
        public long Id { get; set; }
    }
}
