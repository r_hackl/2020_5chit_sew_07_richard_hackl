using Microsoft.VisualStudio.TestTools.UnitTesting;
using _210208_EFCZoo.Models;
using System.Collections.Generic;
using _210208_EFCZoo.Models.Animals;
using System.Linq;
using _210208_EFCZoo.Models.Cages;

namespace UnitTestZoo
{
    [TestClass]
    public class UnitTest1
    {
        public ZooDBContext context = new ZooDBContext();
        
        [TestMethod]
        public void GetAnimalsPerChage()
        {
            long cageId = 2;
            List<AAnimal> animals = new List<AAnimal>();
            animals.AddRange(context.Lions.Where(l => l.Cage == context.preditorCages.Where(c => c.Id == cageId).First() ).ToList());
            animals.AddRange(context.Harpies.Where(l => l.Cage == context.preditorCages.Where(c => c.Id == cageId).First()).ToList());
            Assert.IsTrue(animals.Count() > 0);

        }
        [TestMethod]
        public void GetPredCagesPerLocation()
        {
            ELOCATION location = ELOCATION.SAVANNAH;
            List<PreditorCage> cages = context.preditorCages.Where(c => c.Location == location).ToList();
            Assert.IsTrue(cages.Count() > 0);
        }
        [TestMethod]
        public void GetAllAnimals()
        {
            List<AAnimal> animals = new List<AAnimal>();
            animals.AddRange(context.Gazelles);
            animals.AddRange(context.Harpies);
            animals.AddRange(context.Lions);
            animals.AddRange(context.Sharks);
            animals.AddRange(context.Tortoises);

            Assert.IsTrue(animals.Count() >= 30);
        }
        [TestMethod]
        public void GetAllWaterCages()
        {
            List<WaterCage> cages = context.WaterCages.ToList();
            Assert.IsTrue(cages.Count() > 0);
        }
        [TestMethod]
        public void GetSharksWithToothCountGT20()
        {
            List<Shark> sharks = context.Sharks.Where(s => s.TouthCont > 20).ToList();
            Assert.IsTrue(sharks.Count() > 0);
        }
    }
}
