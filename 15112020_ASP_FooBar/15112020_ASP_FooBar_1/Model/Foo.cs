﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace _15112020_ASP_FooBar_1.Model
{
    public class Foo
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
