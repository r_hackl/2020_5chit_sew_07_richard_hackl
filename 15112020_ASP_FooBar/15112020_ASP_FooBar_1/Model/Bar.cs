﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace _15112020_ASP_FooBar_1.Model
{
    public class Bar
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
