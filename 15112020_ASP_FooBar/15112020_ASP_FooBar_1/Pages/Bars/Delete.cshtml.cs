﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using _15112020_ASP_FooBar_1.Data;
using _15112020_ASP_FooBar_1.Model;

namespace _15112020_ASP_FooBar_1.Pages.Bars
{
    public class DeleteModel : PageModel
    {
        private readonly _15112020_ASP_FooBar_1.Data.FBContext _context;

        public DeleteModel(_15112020_ASP_FooBar_1.Data.FBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Bar Bar { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Bar = await _context.Bar.FirstOrDefaultAsync(m => m.Id == id);

            if (Bar == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Bar = await _context.Bar.FindAsync(id);

            if (Bar != null)
            {
                _context.Bar.Remove(Bar);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
