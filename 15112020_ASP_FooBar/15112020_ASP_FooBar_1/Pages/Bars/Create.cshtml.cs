﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using _15112020_ASP_FooBar_1.Data;
using _15112020_ASP_FooBar_1.Model;

namespace _15112020_ASP_FooBar_1.Pages.Bars
{
    public class CreateModel : PageModel
    {
        private readonly _15112020_ASP_FooBar_1.Data.FBContext _context;

        public CreateModel(_15112020_ASP_FooBar_1.Data.FBContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Bar Bar { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Bar.Add(Bar);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
