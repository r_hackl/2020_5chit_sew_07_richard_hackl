﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using _15112020_ASP_FooBar_1.Data;
using _15112020_ASP_FooBar_1.Model;

namespace _15112020_ASP_FooBar_1.Pages.Foos
{
    public class DeleteModel : PageModel
    {
        private readonly _15112020_ASP_FooBar_1.Data.FBContext _context;

        public DeleteModel(_15112020_ASP_FooBar_1.Data.FBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Foo Foo { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Foo = await _context.Foo.FirstOrDefaultAsync(m => m.Id == id);

            if (Foo == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Foo = await _context.Foo.FindAsync(id);

            if (Foo != null)
            {
                _context.Foo.Remove(Foo);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
