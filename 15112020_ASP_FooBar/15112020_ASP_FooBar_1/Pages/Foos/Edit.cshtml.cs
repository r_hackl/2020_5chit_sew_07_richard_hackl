﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _15112020_ASP_FooBar_1.Data;
using _15112020_ASP_FooBar_1.Model;

namespace _15112020_ASP_FooBar_1.Pages.Foos
{
    public class EditModel : PageModel
    {
        private readonly _15112020_ASP_FooBar_1.Data.FBContext _context;

        public EditModel(_15112020_ASP_FooBar_1.Data.FBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Foo Foo { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Foo = await _context.Foo.FirstOrDefaultAsync(m => m.Id == id);

            if (Foo == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Foo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FooExists(Foo.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool FooExists(int id)
        {
            return _context.Foo.Any(e => e.Id == id);
        }
    }
}
