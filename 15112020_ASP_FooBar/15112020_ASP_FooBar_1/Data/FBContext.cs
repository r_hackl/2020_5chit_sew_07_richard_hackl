﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using _15112020_ASP_FooBar_1.Model;

namespace _15112020_ASP_FooBar_1.Data
{
    public class FBContext : DbContext
    {
        public FBContext (DbContextOptions<FBContext> options)
            : base(options)
        {
        }

        public DbSet<_15112020_ASP_FooBar_1.Model.Bar> Bar { get; set; }

        public DbSet<_15112020_ASP_FooBar_1.Model.Foo> Foo { get; set; }
    }
}
