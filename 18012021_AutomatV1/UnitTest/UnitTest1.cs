﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using _18012021_AutomatV1;
using System.Collections.Generic;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        CoffeeDispenser cdp = new CoffeeDispenser();

        [TestMethod]
        public void TestEspresso()
        {
            Espresso a = (Espresso)cdp.buyProduct(new List<ETopping>(), 
                EType.CAFFEINE_DRINK, 
                0, 
                0, 
                0);
            Assert.IsTrue(a is Espresso);
        }

        [TestMethod]
        public void TestLatte()
        {
            Latte a = (Latte)cdp.buyProduct(new List<ETopping>(),
                EType.HOT_DRINK,
                0,
                0,
                0);
            Assert.IsTrue(a is Latte);
        }

        [TestMethod]
        public void TestOrangeTea()
        {
            OrangeTea a = (OrangeTea)cdp.buyProduct(new List<ETopping>(),
                EType.HOT_DRINK_SWEET,
                0,
                0,
                0);
            Assert.IsTrue(a is OrangeTea);
        }


        [TestMethod]
        [ExpectedException(typeof(OutOfIngredientException))]

        public void TestExceptions()
        {
            cdp.amountOfMilk = 0;
            cdp.amountOfSugar = 0;
            cdp.amountOfCaffeine = 0;

            OrangeTea a = (OrangeTea)cdp.buyProduct(new List<ETopping>(),
                EType.HOT_DRINK_SWEET,
                5,
                5,
                5);
        }
    }
}
