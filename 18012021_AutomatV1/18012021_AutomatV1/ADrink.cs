﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18012021_AutomatV1
{
    public enum EType { CAFFEINE_DRINK = 0, HOT_DRINK, HOT_DRINK_SWEET, COLD_DRINK }
    public enum ETopping { WHIPPED_CREAM = 0, CHOC_SPRINKLES, CARAMEL, NONE}
    public abstract class ADrink
    {
        public EType type { get; set; }
        public float price { get; set; }
        public int amountOfSugar { get; set; }
        public int amountOfCaffeine { get; set; }
        public int amountOfMilk { get; set; }

        public List<ETopping> additionalToppings = new List<ETopping>();

        public void getPrice()
        {
            this.price = 1.0f;

            this.price += amountOfSugar * 0.1f;
            this.price += amountOfCaffeine * 0.1f;
            this.price += amountOfMilk * 0.1f;

            foreach(ETopping topping in additionalToppings)
            {
                this.price += 0.2f;
            }
        }

        public abstract void addMilk(int amount);
        public abstract void addCaffeine(int amount);
        public abstract void addSugar(int amount);
        public abstract void addTopping(ETopping topping);
        public abstract string toString();
    }
}
