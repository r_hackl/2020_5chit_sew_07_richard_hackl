﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18012021_AutomatV1
{
    public class Espresso : ADrink
    {
        public Espresso()
        {
            this.type = EType.CAFFEINE_DRINK;
            this.getPrice();
        }

        public override void addCaffeine(int amount)
        {
            this.amountOfCaffeine += amount;
            this.getPrice();
        }

        public override void addSugar(int amount)
        {
            this.amountOfSugar += amount;
            this.getPrice();
        }

        public override void addMilk(int amount)
        {
            // no milk in espresso
            throw new NotImplementedException();
        }

        public override void addTopping(ETopping topping)
        {
            this.additionalToppings.Add(topping);
        }
        public override string toString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.type.ToString()).Append(" Espresso with ")
                .Append(" sugarAmount: " + amountOfSugar + " ")
                .Append(" milkAmount: " + amountOfMilk + " ")
                .Append(" sugarAmount: " + amountOfCaffeine + " ");

            if(this.additionalToppings.Count < 1) {
                sb.Append("With toppings: ");

                foreach(ETopping topping in this.additionalToppings)
                {
                    sb.Append(" " + topping.ToString() + "");
                }
            }
            else
            {
                sb.Append("With NO toppings.");
            }

            return sb.ToString();
        }
    }
}
