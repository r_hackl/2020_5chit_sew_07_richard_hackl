﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18012021_AutomatV1
{
    public class OrangeTea : ADrink
    {
        public OrangeTea()
        {
            this.type = EType.HOT_DRINK_SWEET;
            this.getPrice();
        }

        public override void addCaffeine(int amount)
        {
            throw new NotImplementedException();
        }

        public override void addSugar(int amount)
        {
            this.amountOfSugar += amount;
        }

        public override void addMilk(int amount)
        {
            // no milk in espresso
            throw new NotImplementedException();
        }

        public override void addTopping(ETopping topping)
        {
            // no toppings on tea
            throw new NotImplementedException();
        }

        public override string toString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.type.ToString()).Append(" Espresso with ")
                .Append(" sugarAmount: " + amountOfSugar + " ")
                .Append(" milkAmount: " + amountOfMilk + " ")
                .Append(" sugarAmount: " + amountOfCaffeine + " ");

            if (this.additionalToppings.Count < 1)
            {
                sb.Append("With toppings: ");

                foreach (ETopping topping in this.additionalToppings)
                {
                    sb.Append(" " + topping.ToString() + "");
                }
            }
            else
            {
                sb.Append("With NO toppings.");
            }

            return sb.ToString();
        }
    }
}
