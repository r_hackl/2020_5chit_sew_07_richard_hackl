﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18012021_AutomatV1
{
    public class CoffeeDispenser
    {
        public int amountOfAmericano { get; set; }
        public int amountOfCapuccino { get; set; }
        public int amountOfEspresso { get; set; }
        public int amountOfOrangeTea { get; set; }
        public int amountOfLatte { get; set; }
        public float accountBalance { get; set; }

        public int amountOfSugar { get; set; }
        public int amountOfCaffeine { get; set; }
        public int amountOfMilk { get; set; }

        public CoffeeDispenser(int _amountOfAmericano = 10,
            int _amountOfCapuccino = 10,
            int _amountOfEspresso = 10,
            int _amountOfOrangeTea = 10,
            int _amountOfLatte = 10
            )
        {
            this.amountOfAmericano = _amountOfAmericano;
            this.amountOfCapuccino = _amountOfCapuccino;
            this.amountOfEspresso = _amountOfEspresso;
            this.amountOfLatte = _amountOfLatte;
            this.amountOfOrangeTea = _amountOfOrangeTea;

            this.amountOfMilk = 10;
            this.amountOfSugar = 10;
            this.amountOfCaffeine = 10;

            // for testing
            this.accountBalance = 2.0f;
        }

        public float getAccountBalance()
        {
            return this.accountBalance;
        }

        public ADrink buyProduct(List<ETopping> toppings, 
            EType type, 
            int _amountOfSugar, 
            int _amountOfCaffeine, 
            int _amountOfMilk)
        {
            ADrink orderedDrink;

            switch (type)
            {
                case EType.CAFFEINE_DRINK:
                    orderedDrink = new Espresso
                    {
                        amountOfSugar = _amountOfSugar,
                        amountOfMilk = _amountOfMilk,
                        amountOfCaffeine = _amountOfCaffeine
                    };

                    this.amountOfMilk -= _amountOfMilk;
                    if(this.amountOfMilk < 0) { throw new OutOfIngredientException(); }

                    this.amountOfSugar -= _amountOfSugar;
                    if (this.amountOfSugar < 0) { throw new OutOfIngredientException(); }

                    this.amountOfCaffeine -= _amountOfCaffeine;
                    if (this.amountOfCaffeine < 0) { throw new OutOfIngredientException(); }

                    orderedDrink.additionalToppings = toppings;
                    this.amountOfEspresso -= 1;
                    break;

                case EType.HOT_DRINK_SWEET:
                    orderedDrink = new OrangeTea
                    {
                        amountOfSugar = _amountOfSugar,
                        amountOfMilk = _amountOfMilk,
                        amountOfCaffeine = _amountOfCaffeine
                    };

                    this.amountOfMilk -= _amountOfMilk;
                    if (this.amountOfMilk < 0) { throw new OutOfIngredientException(); }

                    this.amountOfSugar -= _amountOfSugar;
                    if (this.amountOfSugar < 0) { throw new OutOfIngredientException(); }

                    this.amountOfCaffeine -= _amountOfCaffeine;
                    if (this.amountOfCaffeine < 0) { throw new OutOfIngredientException(); }


                    // empty toppings
                    orderedDrink.additionalToppings = new List<ETopping>();
                    this.amountOfOrangeTea -= 1;
                    break;

                case EType.HOT_DRINK:
                    orderedDrink = new Latte
                    {
                        amountOfSugar = _amountOfSugar,
                        amountOfMilk = _amountOfMilk,
                        amountOfCaffeine = _amountOfCaffeine
                    };

                    this.amountOfMilk -= _amountOfMilk;
                    if (this.amountOfMilk < 0) { throw new OutOfIngredientException(); }

                    this.amountOfSugar -= _amountOfSugar;
                    if (this.amountOfSugar < 0) { throw new OutOfIngredientException(); }

                    this.amountOfCaffeine -= _amountOfCaffeine;
                    if (this.amountOfCaffeine < 0) { throw new OutOfIngredientException(); }


                    orderedDrink.additionalToppings = toppings;
                    this.amountOfLatte -= 1;
                    break;

                case EType.COLD_DRINK:
                    orderedDrink = new Capuccino
                    {
                        amountOfSugar = _amountOfSugar,
                        amountOfMilk = _amountOfMilk,
                        amountOfCaffeine = _amountOfCaffeine
                    };

                    this.amountOfMilk -= _amountOfMilk;
                    if (this.amountOfMilk < 0) { throw new OutOfIngredientException(); }

                    this.amountOfSugar -= _amountOfSugar;
                    if (this.amountOfSugar < 0) { throw new OutOfIngredientException(); }

                    this.amountOfCaffeine -= _amountOfCaffeine;
                    if (this.amountOfCaffeine < 0) { throw new OutOfIngredientException(); }


                    orderedDrink.additionalToppings = toppings;
                    this.amountOfCapuccino -= 1;
                    break;

                default:
                    throw new DrinkDoesNotExistException("Unknown");
                    break;
            }

            if (this.accountBalance < orderedDrink.price)
                throw new NotEnoughBalanceException();

            return orderedDrink;
        }

        public void Pay(ADrink drink)
        {
            this.accountBalance -= drink.price;
        }
    }
}
