﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18012021_AutomatV1
{
    public class Capuccino : ADrink
    {
        public Capuccino()
        {
            this.type = EType.CAFFEINE_DRINK;
            this.getPrice();
        }
        public override void addCaffeine(int amount)
        {
            this.amountOfCaffeine += amount;
            this.getPrice();
        }

        public override void addSugar(int amount)
        {
            this.amountOfSugar += amount;
            this.getPrice();
        }

        public override void addMilk(int amount)
        {
            this.amountOfMilk += amount;
            this.getPrice();
        }

        public override void addTopping(ETopping topping)
        {
            this.additionalToppings.Add(topping);
        }

        public override string toString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.type.ToString()).Append(" Espresso with ")
                .Append(" sugarAmount: " + amountOfSugar + " ")
                .Append(" milkAmount: " + amountOfMilk + " ")
                .Append(" sugarAmount: " + amountOfCaffeine + " ");

            if (this.additionalToppings.Count < 1)
            {
                sb.Append("With toppings: ");

                foreach (ETopping topping in this.additionalToppings)
                {
                    sb.Append(" " + topping.ToString() + "");
                }
            }
            else
            {
                sb.Append(" NO TOPPINGS. ");
            }

            return sb.ToString();
        }
    }
}
