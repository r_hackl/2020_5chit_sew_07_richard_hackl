﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18012021_AutomatV1
{
    public class DrinkDoesNotExistException : Exception
    {
        public DrinkDoesNotExistException()
        {
        }

        public DrinkDoesNotExistException(string drink)
            : base(String.Format("Dispenser cannot create drink: {0}", drink))
        {

        }

        public DrinkDoesNotExistException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class OutOfIngredientException : Exception
    {
        public OutOfIngredientException()
        {
        }

        public OutOfIngredientException(string drink)
            : base(String.Format("Lack of ingredients", drink))
        {

        }

        public OutOfIngredientException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class NoMoreEspressoException : Exception
    {
        public NoMoreEspressoException()
        {
        }

        public NoMoreEspressoException(string drink)
            : base(String.Format("No more espresso powder!", drink))
        {

        }

        public NoMoreEspressoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class NotEnoughBalanceException : Exception
    {
        public NotEnoughBalanceException()
        {
        }

        public NotEnoughBalanceException(string drink)
            : base(String.Format("No enough account balance!", drink))
        {

        }

        public NotEnoughBalanceException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
