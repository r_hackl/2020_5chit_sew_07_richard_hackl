﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP_Übung
{
    public static class DataInitializer
    {
        public static void Seed(CarVM vm)
        {
            var customers = new List<Customer>
            {
                new Customer {FirstName = "Dave", LastName = "Brenner"},
                new Customer {FirstName = "Matt", LastName = "Walton"},
                new Customer {FirstName = "Steve", LastName = "Hagen"},
                new Customer {FirstName = "Pat", LastName = "Walton"},
                new Customer {FirstName = "Bad", LastName = "Customer"},
            };

            var cars = new List<Car>
            {
                new Car {Make = "VW", Color = "Black", CarNickName = "Zippy"},
                new Car {Make = "Ford", Color = "Rust", CarNickName = "Rusty"},
                new Car {Make = "Saab", Color = "Black", CarNickName = "Mel"},
                new Car {Make = "Yugo", Color = "Yellow", CarNickName = "Clunker"},
                new Car {Make = "BMW", Color = "Black", CarNickName = "Bimmer"},
                new Car {Make = "BMW", Color = "Green", CarNickName = "Hank"},
                new Car {Make = "BMW", Color = "Pink", CarNickName = "Pinky"},
                new Car {Make = "Pinto", Color = "Black", CarNickName = "Pete"},
                new Car {Make = "Yugo", Color = "Brown", CarNickName = "Brownie"},
            };

            //Todo: Füge CreditRiskCustomer hinzu.
            var creditRiskCustomers = new List<CreditRisk>
            {
                new CreditRisk{FirstName = "Sepp", LastName = "Franz"},
                new CreditRisk{FirstName = "Ruaschi", LastName = "Kurt"},
                new CreditRisk{FirstName = "Hinterholzer", LastName = "Behm"},
                new CreditRisk{FirstName = "Gundl", LastName = "Huawa"},
            };

            foreach(Customer c in customers)
            {
                vm.Customers.Add(c);
            }
            foreach (Car c in cars)
            {
                vm.Cars.Add(c);
            }
            foreach (CreditRisk c in creditRiskCustomers)
            {
                vm.CreditRisks.Add(c);
            }
        }
    }

}
