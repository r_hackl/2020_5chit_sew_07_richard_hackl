﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP_Übung
{
	public class Car
	{
		public int CarId { get; set; }
		public string Make { get; set; }
		public string Color { get; set; }
		public string CarNickName { get; set; }
	}
	public class CreditRisk
	{
		public int CustId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}

	public class Customer
	{
		public int CustId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string FullName => FirstName + " " + LastName;
		public override string ToString() => FullName;
	}
}
