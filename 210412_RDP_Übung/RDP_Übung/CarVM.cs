﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Collections;
using System.Collections.ObjectModel;

namespace RDP_Übung
{
    public class CarVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Car> Cars { get; set; } = new ObservableCollection<Car>();
        public ObservableCollection<CreditRisk> CreditRisks { get; set; } = new ObservableCollection<CreditRisk>();
        public ObservableCollection<Customer> Customers { get; set; } = new ObservableCollection<Customer>();

        public CarVM()
        {
            //todo
        }

        public void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        
        public ICommand DeleteCustomerCommand { get; set; }
        public ICommand DeleteCreditRiskCommand { get; set; }
        public ICommand DeleteCarCommand { get; set; }
        public ICommand AddCustomerCommand { get; set; }
        public ICommand AddCreditRiskCommand { get; set; }
        public ICommand AddCarCommand { get; set; }


    }
}
