﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RDP_Übung
{
    public class RelayCommand : ICommand
    {
        Action<object> execute;
        Predicate<object> canexecute;

        //public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<object> e, Predicate<object> c)
        {
            execute = e;
            canexecute = c;
        }

        public bool CanExecute(object parameter)
        {
            return canexecute?.Invoke(parameter) ?? true;
        }

        public void Execute(object parameter)
        {
            execute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested = true;
        }
    }
}
