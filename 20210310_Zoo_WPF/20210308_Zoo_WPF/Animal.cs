﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _20210308_Zoo_WPF
{
    public enum EAnimalState { FREE=1, CAGED, SICK, FREED, DEAD}
    public abstract class Animal
    {
        public int Id { get; set; }
        public EAnimalState State { get; set; }
        public int Sickness { get; set; }
        public override string ToString()
        {
            return "Type: " + this.GetType() + " State: " + this.State + " Id: " + this.Id;
        }
    }
    public abstract class DangerousAnimal : Animal
    {
        public int HowDangerous { get; set; }
    }
    public abstract class FriendlyAnimal : Animal
    {
        public int HowFriendly { get; set; }
    }
    public abstract class AquaAnimal : Animal
    {
        public int HowFriendly { get; set; }
    }
    public class Lion : DangerousAnimal
    {
        public int RoardB { get; set; }
        public override string ToString()
        {
            return "Type: " + this.GetType() + " State: " + this.State + " Id: " + this.Id +  " RoardB: " + this.RoardB;
        }
    }
    public class Wolf : DangerousAnimal
    {
        public int GroupSize { get; set; }
        public override string ToString()
        {
            return "Type: " + this.GetType() + " State: " + this.State + " Id: " + this.Id + " GroupSize: " + this.GroupSize;
        }
    }
    public class Dolphin : AquaAnimal
    {
        public string Trick { get; set; }
        public override string ToString()
        {
            return "Type: " + this.GetType() + " State: " + this.State + " Id: " + this.Id + " Trick: " + this.Trick;
        }
    }
    public class Shark : AquaAnimal
    {
        public int KilledHumans { get; set; }
        public override string ToString()
        {
            return "Type: " + this.GetType() + " State: " + this.State + " Id: " + this.Id + " KilledHumans: " + this.KilledHumans;
        }
    }
    public class Goat : FriendlyAnimal
    {
        public bool BothHorns { get; set; }
        public override string ToString()
        {
            return "Type: " + this.GetType() + " State: " + this.State + " Id: " + this.Id + " BothHorns: " + this.BothHorns;
        }
    }
    public class Sheep : FriendlyAnimal
    {
        public bool Black { get; set; }
        public override string ToString()
        {
            return "Type: " + this.GetType() + " State: " + this.State + " Id: " + this.Id;
        }
    }
}
