﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace _20210308_Zoo_WPF
{
    public abstract class CreateCageCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        protected ZooVM parent;

        public CreateCageCommand(ZooVM p)
        {
            parent = p;
        }  

        public virtual void Execute(object parameter) { }

        public bool CanExecute(object parameter)
        {
            return true;
        }
    }
    public class CreateAquaCageCommand : CreateCageCommand
    {
        public CreateAquaCageCommand(ZooVM p) : base(p) { }
        public override void Execute(object parameter)
        {
            parent.CreateAquaCage();
        }
    }
    public class CreateDangerCageCommand : CreateCageCommand
    {
        public CreateDangerCageCommand(ZooVM p) : base(p) { }
        public override void Execute(object parameter)
        {
            parent.CreateDangerCage();
        }
    }
    public class CreateFriendlyCageCommand : CreateCageCommand
    {
        public CreateFriendlyCageCommand(ZooVM p) : base(p) { }
        public override void Execute(object parameter)
        {
            parent.CreateFriendlyCage();
        }
    }

    public class AttachCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        protected ZooVM parent;

        public AttachCommand(ZooVM p)
        {
            parent = p;
        }

        public virtual void Execute(object parameter) 
        {
            parent.Attach();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }
    }
    public class LoadAnimalsOfSelectedCageCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        protected ZooVM parent;

        public LoadAnimalsOfSelectedCageCommand(ZooVM p)
        {
            parent = p;
        }

        public virtual void Execute(object parameter)
        {
            parent.LoadAnimalsOfSelectedCage();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }
    }
    public class ChangeAnimalCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        protected ZooVM parent;

        public ChangeAnimalCommand(ZooVM p)
        {
            parent = p;
        }

        public virtual void Execute(object parameter)
        {
            parent.ChangeAnimal();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }
    }
    
    public class RelayCommand: ICommand
    {
        Action<object> execute;
        Predicate<object> canexecute;

        public RelayCommand(Action<object> e, Predicate<object> c)
        {
            execute = e;
            canexecute = c;
        }
        public virtual void Execute(object parameter)
        {
            execute(parameter);
        }
        public bool CanExecute(object parameter)
        {
            return canexecute?.Invoke(parameter) ?? true;
        }
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested += value;
        }
    }
}
