﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _20210308_Zoo_WPF
{
    public class CageFactory
    {
        public AquaCage CreateAquaCage(int id, int size = 10)
        {
            return new AquaCage(id, size);
        }
        public DangerCage CreateDangerCage(int id, int size = 10)
        {
            return new DangerCage(id, size);
        }
        public FriendlyCage CreateFriendlyCage(int id, int size = 10)
        {
            return new FriendlyCage(id, size);
        }
    }

}
