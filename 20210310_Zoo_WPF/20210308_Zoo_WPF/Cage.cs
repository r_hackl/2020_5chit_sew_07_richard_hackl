﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _20210308_Zoo_WPF
{
    public abstract class Cage
    {
        public int Id { get; set; }
        public int Size { get; set; }
        public Cage(int s)
        {
            Size = s;
        }
        public Cage(int id, int s)
        {
            Id = id;
            Size = s;
        }
        public override string ToString()
        {
            return "Typ: " + this.GetType() + " Id: " + this.Id + " Size: " + this.Size;
        }
    }

    public class AquaCage : Cage
    {
        public virtual List<AquaAnimal> AquaAnimals { get; set; } = new List<AquaAnimal>();
        public AquaCage(int s) : base(s) { }
        public AquaCage(int id, int s) : base(id, s) { }
    }
    public class DangerCage : Cage
    {
        public virtual List<DangerousAnimal> DangerousAnimals { get; set; } = new List<DangerousAnimal>();

        public DangerCage(int s) : base(s) { }
        public DangerCage(int id, int s) : base(id, s) { }
    }
    public class FriendlyCage : Cage
    {
        public virtual List<FriendlyAnimal> FriendlyAnimals { get; set; } = new List<FriendlyAnimal>();
        public FriendlyCage(int s) : base(s) { }
        public FriendlyCage(int id, int s) : base(id, s) { }
    }
}
