﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace _20210308_Zoo_WPF
{
    public class XMLHandler
    {
        public static void WriteToXML(List<Animal> animals)
        {
            XDocument doc;
            doc = XDocument.Load("FreeAnimals.xml");
            if (doc != null)
            {
                doc.Element("FreedAnimals").Add(
                    from a in animals
                    select new XElement(a.GetType().ToString(),
                    new XElement("Id", a.Id),
                    new XElement("State", a.State),
                    new XElement("Sickness", a.Sickness),
                    new XElement("Trick", (a is Dolphin) ? (a as Dolphin).Trick : ""),
                    new XElement("KilledHumans", (a is Shark) ? (a as Shark).KilledHumans.ToString() : ""),
                    new XElement("BothHorns", (a is Goat) ? (a as Goat).BothHorns.ToString() : ""),
                    new XElement("Black", (a is Sheep) ? (a as Sheep).Black.ToString() : ""),
                    new XElement("RoardB", (a is Lion) ? (a as Lion).RoardB.ToString() : ""),
                    new XElement("GroupSize", (a is Wolf) ? (a as Wolf).GroupSize.ToString() : "")
                    )
                );
                doc.Save("FreedAnimals.xml");
            }
            else
            {
                doc.Add(
                new XElement("FreedAnimals",
                from a in animals
                select new XElement(a.GetType().ToString(),
                new XElement("Id", a.Id),
                new XElement("State", a.State),
                new XElement("Sickness", a.Sickness),
                new XElement("Trick", (a is Dolphin) ? (a as Dolphin).Trick : ""),
                new XElement("KilledHumans", (a is Shark) ? (a as Shark).KilledHumans.ToString() : ""),
                new XElement("BothHorns", (a is Goat) ? (a as Goat).BothHorns.ToString() : ""),
                new XElement("Black", (a is Sheep) ? (a as Sheep).Black.ToString() : ""),
                new XElement("RoardB", (a is Lion) ? (a as Lion).RoardB.ToString() : ""),
                new XElement("GroupSize", (a is Wolf) ? (a as Wolf).GroupSize.ToString() : "")
                )
            ));
                doc.Save("FreedAnimals.xml");
            }
        }
    }
}
