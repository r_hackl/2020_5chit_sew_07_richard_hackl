﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace _20210308_Zoo_WPF
{
    public class ZooVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Animal> Animals { get; set; } = new ObservableCollection<Animal>();
        public ObservableCollection<Animal> FreedAnimals { get; set; } = new ObservableCollection<Animal>();
        public ObservableCollection<Animal> AnimalsOfSelectedCage { get; set; } = new ObservableCollection<Animal>();
        public ObservableCollection<Cage> Cages { get; set; } = new ObservableCollection<Cage>();
        public ICollectionView FriendlyCages { get; set; }
        public ICollectionView AquaCages { get; set; }
        public ICollectionView DangerCages { get; set; }
        public ICollectionView SickCages { get; set; }
        public ICollectionView AnimalsOfSelectedFriendlyCage { get; set; }
        public ICollectionView AnimalsOfSelectedDangerCage { get; set; }
        public ICollectionView AnimalsOfSelectedAquaCage { get; set; }
        public ICommand XMLCommand { get; set; }
        public ObservableCollection<EAnimalState> States { get; set; } = new ObservableCollection<EAnimalState>();
        public Animal selectedAnimal { get; set; }
        public Cage selectedCage { get; set; }
        public int currentCageSizeSlider = 10;

        private bool free = true;
        private bool backtocage;
        public bool Free
        {
            get
            {
                return free;
            }
            set
            {
                free = value;
                OnPropertyChanged();
            }
        }
        public bool BackToCage
        {
            get
            {
                return backtocage;
            }
            set
            {
                backtocage = value;
                OnPropertyChanged();
            }
        }
        public int CurrentCageSizeSlider
        {
            get { return currentCageSizeSlider; }
            set
            {
                currentCageSizeSlider = value;
                OnPropertyChanged();
            }
        }

        public int Sickness
        {
            get
            {
                return selectedAnimal!=null ? selectedAnimal.Sickness : 0;
            }
            set
            {
                selectedAnimal.Sickness = value;
                OnPropertyChanged();
            }
        }
        public string DolphinTrick
        {
            get
            {
                return (selectedAnimal is Dolphin) ? (selectedAnimal as Dolphin).Trick : null;
            }
            set
            {
                if (selectedAnimal is Dolphin)
                {
                    (selectedAnimal as Dolphin).Trick = value;
                    OnPropertyChanged();
                }
            }
        }
        public int HowDangerous
        {
            get
            {
                return (selectedAnimal is Shark) ? (selectedAnimal as Shark).KilledHumans : 0;
            }
            set
            {
                if (selectedAnimal is Shark)
                {
                    (selectedAnimal as Shark).KilledHumans = value;
                    OnPropertyChanged();
                }
            }
        }
        public bool FriendlyAnimalAttribute
        {
            get
            {
                if (selectedAnimal is Sheep)
                    return (selectedAnimal as Sheep).Black;
                else if (selectedAnimal is Goat)
                    return (selectedAnimal as Goat).BothHorns;
                else
                    return false;
            }
            set
            {
                if (selectedAnimal is Sheep)
                {
                    (selectedAnimal as Sheep).Black = value;
                    OnPropertyChanged();
                }
                else if (selectedAnimal is Goat)
                {
                    (selectedAnimal as Goat).BothHorns = value;
                    OnPropertyChanged();
                }
            }
        }
        public int DangerousAnimalAttribute
        {
            get
            {
                if (selectedAnimal is Lion)
                    return (selectedAnimal as Lion).RoardB;
                else if (selectedAnimal is Wolf)
                    return (selectedAnimal as Wolf).GroupSize;
                else
                    return 0;
            }
            set
            {
                if (selectedAnimal is Lion)
                {
                    (selectedAnimal as Lion).RoardB = value;
                    OnPropertyChanged();
                }
                else if (selectedAnimal is Wolf)
                {
                    (selectedAnimal as Wolf).GroupSize = value;
                    OnPropertyChanged();
                }
            }
        }

        public CageFactory cageFactory = new CageFactory();
        public ICommand CreateDangerCageCommand { get; set; }
        public ICommand CreateFriendlyCageCommand { get; set; }
        public ICommand CreateAquaCageCommand { get; set; }
        public ICommand AttachCommand { get; set; }
        public ICommand LoadAnimalsOfSelectedCageCommand { get; set; }
        public ICommand ChangeAnimalCommand { get; set; }
        public ICommand HealCommand { get; set; }

        public ZooVM()
        {
            FriendlyCages = new CollectionViewSource { Source = Cages }.View;
            FriendlyCages.Filter += x => x is FriendlyCage;
            DangerCages = new CollectionViewSource { Source = Cages }.View;
            DangerCages.Filter += x => x is DangerCage;
            AquaCages = new CollectionViewSource { Source = Cages }.View;
            AquaCages.Filter += x => x is AquaCage;
            SickCages = new CollectionViewSource { Source = Cages }.View;
            SickCages.Filter += x => (x as Cage).Size == 1;
            AnimalsOfSelectedAquaCage = new CollectionViewSource { Source = AnimalsOfSelectedCage }.View;
            AnimalsOfSelectedAquaCage.Filter += x => x is AquaAnimal;
            AnimalsOfSelectedDangerCage = new CollectionViewSource { Source = AnimalsOfSelectedCage }.View;
            AnimalsOfSelectedDangerCage.Filter += x => x is DangerousAnimal;
            AnimalsOfSelectedFriendlyCage = new CollectionViewSource { Source = AnimalsOfSelectedCage }.View;
            AnimalsOfSelectedFriendlyCage.Filter += x => x is FriendlyAnimal;
            States.Add(EAnimalState.FREE);
            States.Add(EAnimalState.CAGED);
            States.Add(EAnimalState.SICK);
            States.Add(EAnimalState.FREED);
            States.Add(EAnimalState.DEAD);
            CreateDangerCageCommand = new CreateDangerCageCommand(this);
            CreateFriendlyCageCommand = new CreateFriendlyCageCommand(this);
            CreateAquaCageCommand = new CreateAquaCageCommand(this);
            AttachCommand = new AttachCommand(this);
            LoadAnimalsOfSelectedCageCommand = new LoadAnimalsOfSelectedCageCommand(this);
            ChangeAnimalCommand = new ChangeAnimalCommand(this);
            HealCommand = new RelayCommand(x => this.Heal(), x => true);
            XMLCommand = new RelayCommand(x => this.ToXML(), x => true);
            ReadLions("Lions.csv");
            ReadWolves("Wolves.csv");
            ReadGoats("Goats.csv");
            ReadSheeps("Sheeps.csv");
            ReadDolphins("Dolphins.csv");
            ReadSharks("Sharks.csv");
        }

        public EAnimalState CurrentState
        {
            get { return (selectedAnimal != null) ? selectedAnimal.State : (EAnimalState)6; }
            set
            {
                selectedAnimal.State = value;
                OnPropertyChanged();
            }
        }
        public void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public void Attach()
        {
            if(selectedAnimal != null && selectedCage != null)
            {
                if(selectedAnimal is AquaAnimal && selectedCage is AquaCage)
                {
                    if (selectedCage.Size > (selectedCage as AquaCage).AquaAnimals.Count)
                    {
                        if(!((selectedCage as AquaCage).AquaAnimals.Contains(selectedAnimal)))
                        {
                            (selectedCage as AquaCage).AquaAnimals.Add(selectedAnimal as AquaAnimal);
                            selectedAnimal.State = EAnimalState.CAGED;
                        }
                        else
                            MessageBox.Show("Tier ist bereits zugewiesen!");

                    }
                    else
                        MessageBox.Show("Aquarium ist voll!");
                }
                else if (selectedAnimal is DangerousAnimal && selectedCage is DangerCage)
                {
                    if (selectedCage.Size > (selectedCage as DangerCage).DangerousAnimals.Count)
                    {
                        if(!((selectedCage as DangerCage).DangerousAnimals.Contains(selectedAnimal)))
                        {
                            (selectedCage as DangerCage).DangerousAnimals.Add(selectedAnimal as DangerousAnimal);
                            selectedAnimal.State = EAnimalState.CAGED;
                        }
                        else
                            MessageBox.Show("Tier ist bereits zugewiesen!");

                    }
                    else
                        MessageBox.Show("Cage ist voll!");
                }
                else if (selectedAnimal is FriendlyAnimal && selectedCage is FriendlyCage)
                {
                    if (selectedCage.Size > (selectedCage as FriendlyCage).FriendlyAnimals.Count)
                    {
                        if (!((selectedCage as FriendlyCage).FriendlyAnimals.Contains(selectedAnimal)))
                        {
                            (selectedCage as FriendlyCage).FriendlyAnimals.Add(selectedAnimal as FriendlyAnimal);
                            selectedAnimal.State = EAnimalState.CAGED;
                        }
                        else
                            MessageBox.Show("Tier ist bereits zugewiesen!");
                    }
                    else
                        MessageBox.Show("Cage ist voll!");
                }
                else
                {
                    MessageBox.Show("Dieses Tier kann nich zugewiesen werden!");
                }
            }
        }
        public void CreateAquaCage()
        {
            Cages.Add(cageFactory.CreateAquaCage((Cages != null)? Cages.Count : 1, currentCageSizeSlider));
        }
        public void CreateDangerCage()
        {
            Cages.Add(cageFactory.CreateDangerCage((Cages != null) ? Cages.Count : 1, currentCageSizeSlider));

        }
        public void CreateFriendlyCage()
        {
            Cages.Add(cageFactory.CreateFriendlyCage((Cages != null) ? Cages.Count : 1, currentCageSizeSlider));

        }
        public void ReadLions(string path)
        {
            StreamReader sr = new StreamReader(path);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splittedline = line.Split(';');
                Animals.Add(new Lion() { Id = Convert.ToInt32(splittedline[0]), State=(EAnimalState)Convert.ToInt32(splittedline[1]), HowDangerous=Convert.ToInt32(splittedline[2]), RoardB=Convert.ToInt32(splittedline[3])});
            }
            sr.Close();
        }
        public void ReadWolves(string path)
        {
            StreamReader sr = new StreamReader(path);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splittedline = line.Split(';');
                Animals.Add(new Wolf() { Id = Convert.ToInt32(splittedline[0]), State = (EAnimalState)Convert.ToInt32(splittedline[1]), HowDangerous = Convert.ToInt32(splittedline[2]), GroupSize = Convert.ToInt32(splittedline[3]) });
            }
            sr.Close();
        }
        public void ReadGoats(string path)
        {
            StreamReader sr = new StreamReader(path);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splittedline = line.Split(';');
                Animals.Add(new Goat() { Id = Convert.ToInt32(splittedline[0]), State = (EAnimalState)Convert.ToInt32(splittedline[1]), HowFriendly = Convert.ToInt32(splittedline[2]), BothHorns = Convert.ToBoolean(Convert.ToInt32(splittedline[3])) });
            }
            sr.Close();
        }
        public void ReadSheeps(string path)
        {
            StreamReader sr = new StreamReader(path);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splittedline = line.Split(';');
                Animals.Add(new Sheep() { Id = Convert.ToInt32(splittedline[0]), State = (EAnimalState)Convert.ToInt32(splittedline[1]), HowFriendly = Convert.ToInt32(splittedline[2]), Black = Convert.ToBoolean(Convert.ToInt32(splittedline[3])) });
            }
            sr.Close();
        }
        public void ReadDolphins(string path)
        {
            StreamReader sr = new StreamReader(path);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splittedline = line.Split(';');
                Animals.Add(new Dolphin() { Id = Convert.ToInt32(splittedline[0]), State = (EAnimalState)Convert.ToInt32(splittedline[1]), HowFriendly = Convert.ToInt32(splittedline[2]), Trick = splittedline[3] });
            }
            sr.Close();
        }
        public void ReadSharks(string path)
        {
            StreamReader sr = new StreamReader(path);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splittedline = line.Split(';');
                Animals.Add(new Shark() { Id = Convert.ToInt32(splittedline[0]), State = (EAnimalState)Convert.ToInt32(splittedline[1]), HowFriendly = Convert.ToInt32(splittedline[2]), KilledHumans = Convert.ToInt32(splittedline[3]) });
            }
            sr.Close();
        }
        public void LoadAnimalsOfSelectedCage()
        {
            if (selectedCage is FriendlyCage)
            {
                AnimalsOfSelectedCage.Clear();
                foreach (Animal item in (selectedCage as FriendlyCage).FriendlyAnimals)
                {
                    AnimalsOfSelectedCage.Add(item);
                } 
            }
            if (selectedCage is DangerCage)
            {
                AnimalsOfSelectedCage.Clear();
                foreach (Animal item in (selectedCage as DangerCage).DangerousAnimals)
                {
                    AnimalsOfSelectedCage.Add(item);
                }
            }
            if (selectedCage is AquaCage)
            {
                AnimalsOfSelectedCage.Clear();
                foreach (Animal item in (selectedCage as AquaCage).AquaAnimals)
                {
                    AnimalsOfSelectedCage.Add(item);
                }
            }
        }

        public void ChangeAnimal()
        {
            if(selectedAnimal is FriendlyAnimal)
            {
                if (selectedAnimal.State == EAnimalState.SICK)
                {
                    (selectedCage as FriendlyCage).FriendlyAnimals.Remove(selectedAnimal as FriendlyAnimal);
                    selectedCage = cageFactory.CreateFriendlyCage(Cages.Count, 1);
                    Cages.Add(selectedCage);
                    (selectedCage as FriendlyCage).FriendlyAnimals.Add(selectedAnimal as FriendlyAnimal);
                    Sickness = 100;
                    MessageBox.Show("Das Tier wurde zum Tierarzt gebringt");
                }
                else if (selectedAnimal.State == EAnimalState.FREED || selectedAnimal.State == EAnimalState.FREE)
                {
                    (selectedCage as FriendlyCage).FriendlyAnimals.Remove(selectedAnimal as FriendlyAnimal);
                    MessageBox.Show("Das Tier wurde freigelassen");
                }
                else if (selectedAnimal.State == EAnimalState.DEAD)
                {
                    (selectedCage as FriendlyCage).FriendlyAnimals.Remove(selectedAnimal as FriendlyAnimal);
                    MessageBox.Show("Das Tier wurde gestorben");
                }
            }
            else if (selectedAnimal is DangerousAnimal)
            {
                if (selectedAnimal.State == EAnimalState.SICK)
                {
                    (selectedCage as DangerCage).DangerousAnimals.Remove(selectedAnimal as DangerousAnimal);
                    selectedCage = cageFactory.CreateDangerCage(Cages.Count, 1);
                    Cages.Add(selectedCage);
                    (selectedCage as DangerCage).DangerousAnimals.Add(selectedAnimal as DangerousAnimal);
                    Sickness = 100;
                    MessageBox.Show("Das Tier wurde zum Tierarzt gebringt");
                }
                else if (selectedAnimal.State == EAnimalState.FREED || selectedAnimal.State == EAnimalState.FREE)
                {
                    (selectedCage as DangerCage).DangerousAnimals.Remove(selectedAnimal as DangerousAnimal);
                    MessageBox.Show("Das Tier wurde freigelassen");
                }
                else if (selectedAnimal.State == EAnimalState.DEAD)
                {
                    (selectedCage as DangerCage).DangerousAnimals.Remove(selectedAnimal as DangerousAnimal);
                    MessageBox.Show("Das Tier wurde gestorben");
                }
            }
            else if(selectedAnimal is AquaAnimal)
            {
                if (selectedAnimal.State == EAnimalState.SICK)
                {
                    (selectedCage as AquaCage).AquaAnimals.Remove(selectedAnimal as AquaAnimal);
                    selectedCage = cageFactory.CreateAquaCage(Cages.Count, 1);
                    Cages.Add(selectedCage);
                    (selectedCage as AquaCage).AquaAnimals.Add(selectedAnimal as AquaAnimal);
                    Sickness = 100;
                    MessageBox.Show("Das Tier wurde zum Tierarzt gebringt");
                }
                else if (selectedAnimal.State == EAnimalState.FREED || selectedAnimal.State == EAnimalState.FREE)
                {
                    (selectedCage as AquaCage).AquaAnimals.Remove(selectedAnimal as AquaAnimal);
                    MessageBox.Show("Das Tier wurde freigelassen");
                }
                else if (selectedAnimal.State == EAnimalState.DEAD)
                {
                    (selectedCage as AquaCage).AquaAnimals.Remove(selectedAnimal as AquaAnimal);
                    MessageBox.Show("Das Tier wurde gestorben");
                }
            }
            
        }
        public void Heal()
        {
            Sickness = 0;
            if (Sickness <= 0)
            {
                selectedAnimal.State = EAnimalState.FREED;
                if (selectedAnimal is FriendlyAnimal)
                {
                    (selectedCage as FriendlyCage).FriendlyAnimals.Remove(selectedAnimal as FriendlyAnimal);
                    Sickness = 0;
                    if (Free)
                    {
                        selectedAnimal.State = EAnimalState.FREED;
                        FreedAnimals.Add(selectedAnimal);
                        Animals.Remove(selectedAnimal);
                        MessageBox.Show("Tier ist gesund und wurde GEFREED");
                    }
                    else if (BackToCage)
                    {
                        selectedAnimal.State = EAnimalState.CAGED;
                        (Cages.Where(x => x is FriendlyCage).Where(x => x.Size > 0).FirstOrDefault() as FriendlyCage).FriendlyAnimals.Add(selectedAnimal as FriendlyAnimal);
                        MessageBox.Show("Tier ist gesund und wurde eingesperrt");
                    }
                }
                else if (selectedAnimal is DangerousAnimal)
                {
                    (selectedCage as DangerCage).DangerousAnimals.Remove(selectedAnimal as DangerousAnimal);
                    Sickness = 0;
                    if (Free)
                    {
                        selectedAnimal.State = EAnimalState.FREED;
                        FreedAnimals.Add(selectedAnimal);
                        Animals.Remove(selectedAnimal);
                        MessageBox.Show("Tier ist gesund und wurde GEFREED");
                    }
                    else if (BackToCage)
                    {

                        selectedAnimal.State = EAnimalState.CAGED;
                        (Cages.Where(x => x is DangerCage).Where(x => x.Size > 0).FirstOrDefault() as DangerCage).DangerousAnimals.Add(selectedAnimal as DangerousAnimal);
                        MessageBox.Show("Tier ist gesund und wurde eingesperrt");
                    }
                }
                else if (selectedAnimal is AquaAnimal)
                {
                    (selectedCage as AquaCage).AquaAnimals.Remove(selectedAnimal as AquaAnimal);
                    Sickness = 0;
                    if (Free)
                    {
                        selectedAnimal.State = EAnimalState.FREED;
                        FreedAnimals.Add(selectedAnimal);
                        Animals.Remove(selectedAnimal);
                        MessageBox.Show("Tier ist gesund und wurde GEFREED");
                    }
                    else if (BackToCage)
                    {
                        selectedAnimal.State = EAnimalState.CAGED;
                        (Cages.Where(x => x is AquaCage).Where(x => x.Size > 0).FirstOrDefault() as AquaCage).AquaAnimals.Add(selectedAnimal as AquaAnimal);
                        MessageBox.Show("Tier ist gesund und wurde eingesperrt");
                    }
                }
                Cages.Remove(selectedCage);
                selectedCage = null;
                OnPropertyChanged("selectedAnimal");
                OnPropertyChanged("Animals");
                OnPropertyChanged("FreedAnimals");
            }
        }
        public void ToXML()
        {
            MessageBox.Show("Tier wurde der XML hinzugefügt");
            XMLHandler.WriteToXML(FreedAnimals.ToList());
            FreedAnimals.Clear();
        }
    }
}
