﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class Warehouse : AEntity
    {
        public string Address { get; set; }
        //n-m relation
        public IList<WarehouseWarehouseWorker> WarehouseWarehouseWorkers { get; set; }
        public ICollection<Furniture> Furnitures { get; set; }
        public DateTime JoinDate { get; set; }
    }
}
