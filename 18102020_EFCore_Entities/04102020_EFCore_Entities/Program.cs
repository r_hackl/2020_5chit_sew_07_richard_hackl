﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    class Program
    {
        private static void DisplayStates(IEnumerable<EntityEntry> entries)
        {
            foreach (var entry in entries)
            {
                Console.WriteLine($"Entity: {entry.Entity.GetType().Name}, State: { entry.State.ToString()}");
            }
        }
        static void Main(string[] args)
        {
            MyDBContext.Seed();
            using(var ctx = new MyDBContext())
            {
                var Employee = ctx.Employees.First();
                DisplayStates(ctx.ChangeTracker.Entries());
            }

            Console.ReadKey();
        }
    }
}
