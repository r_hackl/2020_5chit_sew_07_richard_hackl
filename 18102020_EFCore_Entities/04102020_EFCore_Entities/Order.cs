﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class Order : AEntity
    {
        public double Price { get; set; }
        public string OrderCode { get; set; }
        public DateTime OrderDate { get; set; }
        public Customer Customer { get; set; }
        // 1 to 1 relation
        public int FurnitureId { get; set; }
        public Furniture Furniture { get; set; }
    }
}
