﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class Customer : Person
    {
        public DateTime JoinDate { get; set; }
        public bool HasCustomerCard { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
