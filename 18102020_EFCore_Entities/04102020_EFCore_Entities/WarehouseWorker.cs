﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class WarehouseWorker : Employee
    {
        //n - m relation
        public IList<WarehouseWarehouseWorker> WarehouseWarehouseWorkers { get; set; }
    }
}
