﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class Warehouse : AEntity
    {
        public string address { get; set; }
        //keine Liste da sonst erfahrungsgemäß NICHTS mehr funktioniert.
        public Employee employees { get; set; }
        //keine Liste da sonst erfahrungsgemäß NICHTS mehr funktioniert.
        public Furniture furnitures { get; set; }
        public DateTime join_date { get; set; }
    }
}
