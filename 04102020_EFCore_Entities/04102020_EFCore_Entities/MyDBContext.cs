﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class MyDBContext : DbContext
    {
        public DbSet<Customer> customers { get; set; }
        public DbSet<Employee> employees { get; set; }
        public DbSet<Furniture> furnitures { get; set; }
        public DbSet<Order> orders { get; set; }
        public DbSet<Warehouse> warehouses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\r1k\Documents\XXXLutz.mdf;Integrated Security=True;Connect Timeout=30");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

        public static void Seed()
        {
            MyDBContext mdbc = new MyDBContext();

            mdbc.customers.Add(new Customer() { has_customer_card = false, name = "Seep" });
            mdbc.customers.Add(new Customer() { has_customer_card = true, name = "Korl" });

            mdbc.furnitures.Add(new Furniture() { collection = "new Fall Collection", price = 2399.99, type = EFurnitureType.Cupboard });
            mdbc.furnitures.Add(new Furniture() { collection = "new Spring Collection", price = 1499.99, type = EFurnitureType.Chair });
            mdbc.furnitures.Add(new Furniture() { collection = "new Winter Collection", price = 1599.99, type = EFurnitureType.Table });

            mdbc.employees.Add(new Employee() { employment_date = DateTime.Now, job=EJob.Seller, name="Hauns"});
            mdbc.employees.Add(new Employee() { employment_date = DateTime.Now, job=EJob.WarehouseWorker, name = "Hofi" });

            mdbc.SaveChanges();
        }

        public static void SeedFromCSV()
        {
            //List<string> listA = new List<string>();
            //seed data into database
            MyDBContext mdbc = new MyDBContext();

            using (var reader = new StreamReader(@"C:\Users\r1k\source\repos\04102020_EFCore_Entities\furniture.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    mdbc.furnitures.Add(new Furniture() { price = Convert.ToDouble(values[0]), collection = values[1], type = (EFurnitureType)Convert.ToInt32(values[2]) });
                }
                mdbc.SaveChanges();
            }
        }
    }
}
