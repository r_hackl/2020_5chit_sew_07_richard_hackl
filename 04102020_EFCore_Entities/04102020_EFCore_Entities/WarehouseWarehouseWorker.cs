﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class WarehouseWarehouseWorker { 
        public int WarehouseWorkerId { get; set; }
        public WarehouseWorker WarehouseWorker { get; set; }

        public int WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }
    }
}
