﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public enum EJob { Seller = 0, Cashier, HeadOfDepartment, WarehouseWorker}
    public class Employee : AEntity
    {
        public string name { get; set; }
        public DateTime employment_date { get; set; }
        public EJob job { get; set; }
    }
}
