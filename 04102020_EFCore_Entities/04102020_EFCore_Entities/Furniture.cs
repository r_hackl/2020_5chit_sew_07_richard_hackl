﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public enum EFurnitureType { Chair, Table, Cupboard, Bed}
    public class Furniture : AEntity
    {
        public double price { get; set; }
        public string collection { get; set; }
        public EFurnitureType type { get; set; }
    }
}
