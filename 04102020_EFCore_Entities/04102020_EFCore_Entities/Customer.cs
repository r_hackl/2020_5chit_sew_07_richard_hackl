﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class Customer : AEntity
    {
        public string name { get; set; }
        public DateTime join_date { get; set; }
        public bool has_customer_card { get; set; }
    }
}
