﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04102020_EFCore_Entities
{
    public class Order : AEntity
    {
        public double price { get; set; }
        public string order_code { get; set; }
        public DateTime order_date { get; set; }
        public Customer customer { get; set; }
        public Furniture furniture { get; set; }
    }
}
