#pragma checksum "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\Pages\Furniture\FurnitureUpdate.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9537bfa40d8612dc41bba30d7a2b32a9ae9c4e29"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace _20201220_XXXLutz_Blazor.Pages.Furniture
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using _20201220_XXXLutz_Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using _20201220_XXXLutz_Blazor.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\_Imports.razor"
using _13122020_ASP_XXXLutz.Model;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Furniture/update/{id:int}")]
    public partial class FurnitureUpdate : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 24 "C:\Users\r1k\source\repos\20201220_XXXLutz_Blazor\20201220_XXXLutz_Blazor\Pages\Furniture\FurnitureUpdate.razor"
       
    private Furniture furniture;
    [Parameter]
    public int id { set { furniture = c.Furnitures.Find(value); } }
    private async Task Save(MouseEventArgs e)
    {
        await c.SaveChangesAsync();
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private MyDbContext c { get; set; }
    }
}
#pragma warning restore 1591
