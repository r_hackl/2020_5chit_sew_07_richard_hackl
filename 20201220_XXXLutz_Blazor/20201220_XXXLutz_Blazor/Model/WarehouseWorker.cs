﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _13122020_ASP_XXXLutz.Model
{
    [Table("WarehouseWorker")]
    public class WarehouseWorker : Employee
    {
        //n - m relation
        public string AufgabenBereich { get; set; }
        public IList<WarehouseWarehouseWorker> WarehouseWarehouseWorkers { get; set; }
    }
}
