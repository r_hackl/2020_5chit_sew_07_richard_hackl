﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13122020_ASP_XXXLutz.Model
{
    public abstract class AEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
