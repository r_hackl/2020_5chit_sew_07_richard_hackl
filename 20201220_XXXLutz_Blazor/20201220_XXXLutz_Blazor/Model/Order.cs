﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace _13122020_ASP_XXXLutz.Model
{
    public class Order : AEntity
    {
        public double Price { get; set; }
        public string OrderCode { get; set; }
        public DateTime OrderDate { get; set; }
        public Customer Customer { get; set; }
        // 1 to 1 relation
        public int FurnitureId { get; set; }
        public Furniture Furniture { get; set; }
    }
}
