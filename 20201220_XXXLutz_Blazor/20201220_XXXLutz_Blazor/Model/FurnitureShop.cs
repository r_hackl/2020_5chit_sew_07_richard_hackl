﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _13122020_ASP_XXXLutz.Model
{
    public class FurnitureShop : AEntity
    {
        [Required]
        public string Address { get; set; }

        //n-m relation
        public IList<FurnitureShopWorker> FurnitureShopWorkers { get; set; }
    }
}
