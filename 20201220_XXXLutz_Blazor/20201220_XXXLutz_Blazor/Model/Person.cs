﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _13122020_ASP_XXXLutz.Model
{
    public class Person : AEntity
    {
        [Required]
        [Column("C_NAME", Order = 1, TypeName = "nvarchar(128)")]
        public string Name { get; set; }
    }
}
