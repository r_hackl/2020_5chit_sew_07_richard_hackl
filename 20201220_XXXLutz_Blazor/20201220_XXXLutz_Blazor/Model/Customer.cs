﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _13122020_ASP_XXXLutz.Model
{
    [Table("Customer")]
    public class Customer : Person
    {
        public DateTime JoinDate { get; set; }
        public bool HasCustomerCard { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
