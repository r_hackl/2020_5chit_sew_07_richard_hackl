﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _13122020_ASP_XXXLutz.Model
{
    public enum EJob { Seller = 0, Cashier, HeadOfDepartment, WarehouseWorker}
    [Table("Employee")]
    public class Employee : Person
    {
        public DateTime EmploymentDate { get; set; }

        [Required]
        public EJob Job { get; set; }
    }
}
