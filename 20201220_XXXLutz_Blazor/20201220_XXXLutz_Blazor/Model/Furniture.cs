﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace _13122020_ASP_XXXLutz.Model
{
    public enum EFurnitureType { Chair, Table, Cupboard, Bed}
    public class Furniture : AEntity
    {
        public double Price { get; set; }
        public string Collection { get; set; }
        public EFurnitureType Type { get; set; }
        public Order Order { get; set; }
        public Warehouse Warehouse { get; set; }
    }
}
